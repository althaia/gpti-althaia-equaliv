# GPTI - Guia Pratico de Tecnologia da Informação

Este guia foi criado afim de abordar temas práticos aos usuários da empresa Althaia | Equaliv, levando a eles sempre dicas para o dia a dia, e facilidades com o uso da tecnologia.

Abaixo estão os temas abordados em cada versão do guia.

### 27 GPTI ###

* Novidades intranet
* ServiceDesk
* Como reduzir impressão

### 21 GPTI ###

* ServiceDesk - Althaia | Equaliv 4 mil chamados finalizado
* Novidades: Intranet
* Dicas para você zerar a sua caixa de entrada
* Dicas Excel: PROCV
* Wi-Fi não funciona? Veja possíveis causas e soluções para o problema
* Dica: Nova forma de acessar os seus e-mails